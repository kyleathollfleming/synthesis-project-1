//In here should be all the resources that I intend to use?

provider "aws" {
    region = var.aws_region

  # Configuration options
}

terraform {
    backend "s3" {
        bucket = "kyle-terraform-state"
        key = ""
        dynamodb_table = "kyle-terraform-lock"
        region = "eu-west-1"
        encrypt = false

      
    }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.73.0"
    }
  }
}

