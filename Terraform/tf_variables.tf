variable "aws_region"{
  description = "Region for all resources"
  type = string
  default = "eu-west-1"
}

variable "app_name" {
  default = "card-api"
}

variable "api_lambda_functions" {
  default = [
      "get-image",
      "get-image-by-id"
  ]
}

