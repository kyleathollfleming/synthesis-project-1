//openAPI implementation of the api gateway
/*
TODO: 

  Dont make the same mistake as previously, store the image in s3 and the text in dynamodb
  configure api gateway
  configure s3 for images
  create a lambda that triggers when an image is uploaded to the s3
  configure another lambda that sends the image data to the api when the api calls
  figure out how Rekognition works with terraform?

*/

/*
The API gateway must expose a port that a getbyID request can be sent to in order to return the image data
that is stored in the dynamodb after processing.
*/
resource "aws_apigatewayv2_api" "KyleApiGateway" {
  name          = "serverless_lambda_gw"
  protocol_type = "HTTP"

  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}


resource "aws_apigatewayv2_stage" "KyleApiLambdaStage" {
  api_id = aws_apigatewayv2_api.lambda.id

  name        = "serverless_lambda_stage"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}

resource "aws_apigatewayv2_integration" "KyleApiIntegration" {
  api_id = aws_apigatewayv2_api.lambda.id

  integration_uri    = aws_lambda_function.hello_world.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}

resource "aws_apigatewayv2_route" "KyleApiRoute" {
  api_id = aws_apigatewayv2_api.lambda.id

  route_key = "GET /hello"
  target    = "integrations/${aws_apigatewayv2_integration.hello_world.id}"
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.lambda.name}"

  retention_in_days = 30
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello_world.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.lambda.execution_arn}/*/*"
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}




//unsure how to program this at the moment
/*
Goal of using this is to send a notification when the image is successfully analysed and stored 
by the db
*/
module "sns" {
  source  = "terraform-aws-modules/sns/aws"
  version = "3.3.0"
  name = "snsImageUploaded"
tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }  # insert the 18 required variables here
}

//is there some way to explicity say this is for images only?
/*
The s3 holds all the uploaded images and lets Rekognition 
scan them to extract the text on them to be put into the db
*/
resource "aws_s3_bucket" "KyleImageStorage" {
  bucket = "Kyle-Image-Bucket"
  acl    = "public"

  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}

//unsure if I need this
/*
The idea for this resource is to use it to isolate my resources neatly away from the rest of the 
aws resources on the account.
*/
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.11.3"
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }  
  # insert the 23 required variables here
}

/*
This lambda should have the code that allows rekognition to analyse the image.
Another way I think it may work is that rekognition is invoked by this lambda.
*/
module "lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "2.33.2" 
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
  # insert the 32 required variables here
}   

/*
This lambda will process the api call from the gateway and retrieve the data from the dynamodb
*/
module "lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "2.33.2"
  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
  # insert the 32 required variables here
}

//Dynamodb stores the text extracted after the Rekognition service finishes running
resource "aws_dynamodb_table" "Kyle_Dynamodb" {
  name           = "Image_Storage"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "UserId"
  range_key      = "ImageTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "Name"
    type = "S"
  }

  attribute {
    name = "Surname"
    type = "N"
  }

  attribute {
    name = "Skills"
    type = "N"
  }
  attribute {
    name = "Hobbies"
    type = "N"
  }
  attribute {
    name = "Coffee order"
    type = "N"
  }
  attribute {
    name = "Best dad joke"
    type = "N"
  }

  attribute {
    name = "Favourite meal"
    type = "N"
  }
  attribute {
    name = "Biography"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  global_secondary_index {
    name               = "GameTitleIndex"
    hash_key           = "GameTitle"
    range_key          = "TopScore"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["UserId"]
  }

  tags = {
    Name        = "KyleFleming"
    Environment = "Dev"
  }
}